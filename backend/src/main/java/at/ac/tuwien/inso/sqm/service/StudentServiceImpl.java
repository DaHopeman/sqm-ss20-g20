package at.ac.tuwien.inso.sqm.service;

import at.ac.tuwien.inso.sqm.dto.SemesterDto;
import at.ac.tuwien.inso.sqm.entity.Semester;
import at.ac.tuwien.inso.sqm.entity.StduyPlanEntity;
import at.ac.tuwien.inso.sqm.entity.StudentEntity;
import at.ac.tuwien.inso.sqm.entity.StudyPlanRegistration;
import at.ac.tuwien.inso.sqm.entity.UserAccountEntity;
import at.ac.tuwien.inso.sqm.repository.StduentRepository;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StudentServiceImpl implements StudentServiceInterface {


  private static final Logger LOG = LoggerFactory.getLogger(StudentServiceImpl.class);

  @Autowired
  private StduentRepository stduentRepository;

  @Autowired
  private SemesterServiceInterface semesterService;

  @Override
  @Transactional
  public StudentEntity findOne(Long id) {
    LOG.info("finding student by id {}", id);
    return stduentRepository.findOne(id);
  }

  @Override
  public StudentEntity findOne(UserAccountEntity account) {
    LOG.info("finding student by account {}", account);
    return stduentRepository.findByAccount(account);
  }

  @Override
  public StudentEntity findByUsername(String username) {
    String safeUsername = username.replaceAll("[\n|\r|\t]", "_");
    LOG.info("finding student by username {}", safeUsername);
    return stduentRepository.findByUsername(username);
  }

  @Override
  @Transactional(readOnly = true)
  public List<StudyPlanRegistration> findStudyPlanRegistrationsFor(StudentEntity student) {
    LOG.info("finding studyplanregistrations for student {}", student);
    return student.getStudyplans();
  }

  @Override
  @Transactional
  public void registerStudentToStudyPlan(StudentEntity student, StduyPlanEntity studyPlan) {
    LOG.info(
        "for current semester, registering student {} to StudyPlanEntity {}",
        student, studyPlan);

    SemesterDto semester = semesterService.getOrCreateCurrentSemester();

    registerStudentToStudyPlan(student, studyPlan, semester);
  }

  @Override
  @Transactional
  public void registerStudentToStudyPlan(StudentEntity student, StduyPlanEntity studyPlan,
      SemesterDto currentSemesterDto) {
    LOG.info("for semester {} registering student {} to StudyPlanEntity {}",
        currentSemesterDto, student, studyPlan);

    Semester currentSemester = currentSemesterDto.toEntity();

    StudyPlanRegistration studyPlanRegistration = new StudyPlanRegistration(studyPlan,
        currentSemester);

    student.addStudyplans(studyPlanRegistration);

    stduentRepository.save(student);
  }
}
