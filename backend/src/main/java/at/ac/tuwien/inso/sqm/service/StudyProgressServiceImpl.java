package at.ac.tuwien.inso.sqm.service;

import at.ac.tuwien.inso.sqm.dto.SemesterDto;
import at.ac.tuwien.inso.sqm.entity.Feedback;
import at.ac.tuwien.inso.sqm.entity.Grade;
import at.ac.tuwien.inso.sqm.entity.Lehrveranstaltung;
import at.ac.tuwien.inso.sqm.entity.SemestreTypeEnum;
import at.ac.tuwien.inso.sqm.entity.StudentEntity;
import at.ac.tuwien.inso.sqm.entity.StudyPlanRegistration;
import at.ac.tuwien.inso.sqm.service.study_progress.CourseRegistrationState;
import at.ac.tuwien.inso.sqm.service.study_progress.CuorseRegistration;
import at.ac.tuwien.inso.sqm.service.study_progress.SemesterProgress;
import at.ac.tuwien.inso.sqm.service.study_progress.StudyProgress;
import at.ac.tuwien.inso.sqm.service.study_progress.StudyProgressService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StudyProgressServiceImpl implements StudyProgressService {

  @Autowired
  private SemesterServiceInterface semesterService;

  @Autowired
  private LehrveranstaltungServiceInterface courseService;

  @Autowired
  private GradeIService gradeService;

  @Autowired
  private FeedbackIService feedbackService;

  @Override
  @Transactional(readOnly = true)
  public StudyProgress studyProgressFor(StudentEntity student) {
    SemesterDto currentSemester = semesterService.getOrCreateCurrentSemester();

    List<SemesterDto> semesters = studentSemesters(student);
    List<Lehrveranstaltung> courses = courseService.findAllForStudent(student);
    List<Grade> grades = gradeService.findAllOfStudent(student);
    List<Feedback> feedbacks = feedbackService.findAllOfStudent(student);

    List<SemesterProgress> semestersProgress = semesters.stream()
        .map(it -> new SemesterProgress(it,
            courseRegistrations(it, currentSemester, courses, grades, feedbacks)))
        .collect(Collectors.toList());

    return new StudyProgress(currentSemester, semestersProgress);
  }

  private List<SemesterDto> studentSemesters(StudentEntity student) {
    SemesterDto firstSem = getFirstSemesterFor(student);
    if (firstSem != null) {
      return semesterService.findAllSince(getFirstSemesterFor(student));
    } else {
      return new ArrayList<>();
    }
  }

  /**
   * Get the first semester the student registered for
   * TODO: WTF does this code? Please clean up!
   */
  private SemesterDto getFirstSemesterFor(StudentEntity student) {
    List<StudyPlanRegistration> registrations = student.getStudyplans();

    // TODO refactor: this is not a valid way to treat Semesters
    SemesterDto min = new SemesterDto(Integer.MAX_VALUE, SemestreTypeEnum.SUMMER_SEMESTER);
    min.setId(Long.MAX_VALUE);

    for (StudyPlanRegistration spr : registrations) {
      if (min != null && spr != null && spr.getRegisteredSince() != null && min.getId() > spr
          .getRegisteredSince().getId()) {
        min = spr.getRegisteredSince().toDto();
      }
    }

    if (min == null || min.getId() == Long.MAX_VALUE) {
      return null;
    }
    return min;

  }

  private List<CuorseRegistration> courseRegistrations(SemesterDto semester,
      SemesterDto currentSemester, List<Lehrveranstaltung> courses, List<Grade> grades,
      List<Feedback> feedbacks) {
    return courses.stream()
        .filter(it -> it.getSemester().toDto().equals(semester))
        .map(it -> new CuorseRegistration(it,
            courseRegistrationState(it, currentSemester, grades, feedbacks),
            courseGrade(grades, it)))
        .collect(Collectors.toList());
  }

  private CourseRegistrationState courseRegistrationState(Lehrveranstaltung course,
      SemesterDto currentSemester, List<Grade> grades, List<Feedback> feedbacks) {
    Optional<Grade> grade = grades.stream().filter(it -> it.getCourse().equals(course)).findFirst();
    Optional<Feedback> feedback = feedbacks.stream().filter(it -> it.getCourse().equals(course))
        .findFirst();

    if (feedback.isPresent() && grade.isPresent()) {
      return grade.get().getMark().isPositive() ? CourseRegistrationState.COMPLETE_OK
          : CourseRegistrationState.COMPLETE_NOT_OK;
    } else if (feedback.isPresent()) {
      return CourseRegistrationState.NEEDS_GRADE;
    } else if (grade.isPresent()) {
      return CourseRegistrationState.NEEDS_FEEDBACK;
    } else {
      return course.getSemester().toDto().equals(currentSemester)
          ? CourseRegistrationState.IN_PROGRESS : CourseRegistrationState.NEEDS_FEEDBACK;
    }
  }

  private Grade courseGrade(List<Grade> grades, Lehrveranstaltung course) {
    Optional<Grade> grade = grades.stream().filter(it -> it.getCourse().equals(course)).findFirst();
    if (grade.isPresent()) {
      return grade.get();
    } else {
      return null;
    }
  }
}
