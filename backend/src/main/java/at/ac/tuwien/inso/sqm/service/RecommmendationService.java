package at.ac.tuwien.inso.sqm.service;

import static java.util.function.Function.identity;

import at.ac.tuwien.inso.sqm.entity.Lehrveranstaltung;
import at.ac.tuwien.inso.sqm.entity.StudentEntity;
import at.ac.tuwien.inso.sqm.repository.CourseRepository;
import at.ac.tuwien.inso.sqm.service.course_recommendation.CourseRelevanceFilter;
import at.ac.tuwien.inso.sqm.service.course_recommendation.CourseScorer;
import at.ac.tuwien.inso.sqm.service.course_recommendation.RecommendationIService;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecommmendationService implements RecommendationIService {

  private static final Long N_MAX_COURSE_RECOMMENDATIONS = 10L;

  @Autowired
  private CourseRepository courseRepository;

  @Autowired
  private CourseNormalizer courseNormalizer;

  private List<CourseRelevanceFilter> courseRelevanceFilters;

  private List<CourseScorer> courseScorers;
  private double courseScorersWeights;

  @Autowired
  public RecommmendationService setCourseRelevanceFilters(
      List<CourseRelevanceFilter> courseRelevanceFilters) {
    this.courseRelevanceFilters = courseRelevanceFilters;
    return this;
  }

  @Autowired
  public RecommmendationService setCourseScorers(List<CourseScorer> courseScorers) {
    this.courseScorers = courseScorers;
    courseScorersWeights = courseScorers.stream().mapToDouble(CourseScorer::weight).sum();
    return this;
  }

  @Override
  public List<Lehrveranstaltung> recommendCoursesSublist(StudentEntity student) {
    List<Lehrveranstaltung> recommended = recommendCourses(student);
    return recommended.subList(0, min(N_MAX_COURSE_RECOMMENDATIONS.intValue(), recommended.size()));
  }

  private int min(int a, int b) {
    return Math.min(a, b);
  }

  @Override
  public List<Lehrveranstaltung> recommendCourses(StudentEntity student) {
    List<Lehrveranstaltung> courses = getRecommendableCoursesFor(student);

    // Compute initial scores
    Map<CourseScorer, Map<Lehrveranstaltung, Double>> scores = courseScorers.stream()
        .collect(Collectors.toMap(identity(), it -> it.score(courses, student)));

    // Normalize scores
    scores.values().forEach(it -> courseNormalizer.normalize(it));

    // Aggregate scores, by scorer weights
    Map<Lehrveranstaltung, Double> recommendedCourseMap = courses.stream()
        .collect(Collectors.toMap(identity(), course -> {
          double aggregatedScore = scores.keySet().stream()
              .mapToDouble(scorer -> scores.get(scorer).get(course) * scorer.weight()).sum();
          return aggregatedScore / courseScorersWeights;
        }));

    // Sort courses by score
    return recommendedCourseMap.entrySet().stream()
        .sorted(Map.Entry.<Lehrveranstaltung, Double>comparingByValue().reversed())
        .map(Map.Entry::getKey).collect(Collectors.toList());
  }

  private List<Lehrveranstaltung> getRecommendableCoursesFor(StudentEntity student) {
    List<Lehrveranstaltung> courses = courseRepository.findAllRecommendableForStudent(student);

    for (CourseRelevanceFilter filter : courseRelevanceFilters) {
      courses = filter.filter(courses, student);
    }

    return courses;
  }
}
