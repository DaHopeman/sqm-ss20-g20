package at.ac.tuwien.inso.sqm.dto;

public class BaseDto {

  protected Long id;

  public BaseDto() {
  }

  public BaseDto(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
