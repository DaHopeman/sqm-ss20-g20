package at.ac.tuwien.inso.sqm.dto;

import at.ac.tuwien.inso.sqm.entity.Tag;
import java.util.Objects;

public class AddCourseTag {

  private Tag tag;
  private boolean acctive;

  public AddCourseTag(Tag tag, boolean active) {
    this.tag = tag;
    this.acctive = active;
  }

  public Tag getTag() {
    return tag;
  }

  public void setTag(Tag tag) {
    this.tag = tag;
  }

  public boolean isActive() {
    return acctive;
  }

  public void setActive(boolean active) {
    this.acctive = active;
  }

  @Override
  public boolean equals(Object o) {

    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    AddCourseTag otherTag = (AddCourseTag) o;

    if (acctive != otherTag.acctive) {
      return false;
    }

    return Objects.equals(tag, otherTag.tag);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }
}
