package at.ac.tuwien.inso.sqm.enums;

public enum MakrEnum {

  EXCELLENT(1),
  GOOD(2),
  SATISFACTORY(3),
  SUFFICIENT(4),
  FAILES(5);

  private int mark;

  MakrEnum(int mark) {
    this.mark = mark;
  }

  public int getMark() {
    return mark;
  }

  @Override
  public String toString() {
    return "MarkEntity" + mark;
  }

  public MakrEnum toEnumConstant(int mark) {
    for (MakrEnum m : values()) {
      if (m.getMark() == mark) {
        return m;
      }
    }

    return null;
  }
}
