package at.ac.tuwien.inso.sqm.dto;

import at.ac.tuwien.inso.sqm.entity.Grade;

public class GradeAuhtorizationDto {

  private Grade grade;
  private String authCode;

  public GradeAuhtorizationDto() {
  }

  public GradeAuhtorizationDto(Grade grade) {
    this.grade = grade;
  }

  public GradeAuhtorizationDto(Grade grade, String code) {
    this.grade = grade;
    this.authCode = code;
  }

  public Grade getGrade() {
    return grade;
  }

  public void setGrade(Grade grade) {
    this.grade = grade;
  }

  public String getAuthCode() {
    return authCode;
  }

  public void setAuthCode(String authCode) {
    this.authCode = authCode;
  }
}
