package at.ac.tuwien.inso.sqm.controller;

import static at.ac.tuwien.inso.sqm.controller.Constants.FLASH_MSG;
import static at.ac.tuwien.inso.sqm.controller.Constants.REDIRECT_LOGIN;

import at.ac.tuwien.inso.sqm.entity.UserAccountEntity;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LoginController {

  private static final Logger log = LoggerFactory.getLogger(LoginController.class);

  @GetMapping("/login")
  public String getLogin(
      @RequestParam(value = "loggedOut", required = false) String loggedOut,
      @RequestParam(value = "error", required = false) String error,
      @RequestParam(value = "invalidSession", required = false) String invalidSession,
      @RequestParam(value = "notLoggedIn", required = false) String notLoggedIn,
      HttpSession session,
      HttpServletRequest request,
      Model model,
      RedirectAttributes redirectAttributes
  ) {

    // Show a flash messages after a logout
    if (loggedOut != null) {
      redirectAttributes.addFlashAttribute(FLASH_MSG, "login.loggedOut");
      return REDIRECT_LOGIN;
    }

    // Show a message after an error
    if (error != null) {
      Exception lastException = (Exception) session.getAttribute("SPRING_SECURITY_LAST_EXCEPTION");

      if (lastException != null) {
        redirectAttributes.addFlashAttribute("loginError", lastException.getLocalizedMessage());
      }

      log.info("Removing ?error parameter from /login");
      return REDIRECT_LOGIN;
    }

    // Show a message after the session became invalid
    if (invalidSession != null) {
      redirectAttributes.addFlashAttribute(FLASH_MSG, "login.invalidSession");
      log.info("Removing ?invalidSession parameter from /login");
      return REDIRECT_LOGIN;
    }

    // Show a message, that the user has to login first
    if (notLoggedIn != null) {
      redirectAttributes.addFlashAttribute(FLASH_MSG, "login.notLoggedIn");
      log.info("Removing ?notLoggedIn parameter from /login");
      return REDIRECT_LOGIN;
    }

    // Redirect to "/" if the user is already logged in
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    Object userAccount = auth.getPrincipal();

    if (userAccount instanceof UserAccountEntity) {
      UserAccountEntity castUserAccount = (UserAccountEntity) userAccount;
      Long userId = castUserAccount.getId();

      if (userId != null && userId > 0) {
        log.info("User {} is already logged in", userId);
        redirectAttributes.addFlashAttribute(FLASH_MSG, "login.already-logged-in");
        return "redirect:/";
      }
    }

    return "login";
  }
}
