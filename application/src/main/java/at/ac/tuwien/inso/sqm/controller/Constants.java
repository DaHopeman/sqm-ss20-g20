package at.ac.tuwien.inso.sqm.controller;

public class Constants {

  private Constants() {
    throw new IllegalStateException("Utility class");
  }

  public static final int MAX_PAGE_SIZE = 10;

  public static final String ERROR_VIEW_NAME = "error";
  public static final String COURSE = "course";

  public static final String FLASH_MSG = "flashMessage";
  public static final String FLASH_MESSAGE_NOT_LOCALIZED = "flashMessageNotLocalized";

  //redirects
  public static final String REDIRECT_LOGIN = "redirect:/login";
  public static final String REDIRECT_ADMIN_STUDYPLANS = "redirect:/admin/studyplans";
  public static final String REDIRECT_ADMIN_SUBJECTS = "redirect:/admin/subjects";
  public static final String REDIRECT_ADMIN_USERS = "redirect:/admin/users";
  public static final String REDIRECT_LECTURER_COURSES = "redirect:/lecturer/courses";
}
