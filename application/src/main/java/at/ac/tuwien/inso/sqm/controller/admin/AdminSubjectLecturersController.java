package at.ac.tuwien.inso.sqm.controller.admin;

import static at.ac.tuwien.inso.sqm.controller.Constants.FLASH_MSG;
import static at.ac.tuwien.inso.sqm.controller.Constants.REDIRECT_ADMIN_SUBJECTS;

import at.ac.tuwien.inso.sqm.controller.admin.forms.AddLecturersToSubjectForm;
import at.ac.tuwien.inso.sqm.entity.LecturerEntity;
import at.ac.tuwien.inso.sqm.entity.Subjcet;
import at.ac.tuwien.inso.sqm.exception.LecturerNotFoundException;
import at.ac.tuwien.inso.sqm.exception.RelationNotfoundException;
import at.ac.tuwien.inso.sqm.exception.SubjectNotFoundException;
import at.ac.tuwien.inso.sqm.service.SubjectIService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/admin/subjects/{subjectId}")
public class AdminSubjectLecturersController {

  @Autowired
  private SubjectIService subjectService;

  @ModelAttribute("subject")
  private Subjcet getSubject(@PathVariable Long subjectId) {
    return subjectService.findOne(subjectId);
  }

  @GetMapping(value = "/availableLecturers.json")
  @ResponseBody
  public List<LecturerEntity> getAvailableLecturers(
      @PathVariable Long subjectId,
      @RequestParam(value = "search", defaultValue = "") String search
  ) {
    return subjectService.getAvailableLecturersForSubject(subjectId, search);
  }

  @PostMapping("/lecturers")
  public String addLecturer(
      @PathVariable Long subjectId,
      @Valid AddLecturersToSubjectForm addLecturersToSubjectForm,
      RedirectAttributes redirectAttributes
  ) {

    try {
      Long lecturerUisUserId = addLecturersToSubjectForm.toLecturerId();
      LecturerEntity lecturer = subjectService.addLecturerToSubject(subjectId, lecturerUisUserId);
      String name = lecturer.getName();
      String msg = String.format("admin.subjects.lecturerAdded(${'%s'})", name);
      redirectAttributes.addFlashAttribute(FLASH_MSG, msg);
    } catch (LecturerNotFoundException e) {
      redirectAttributes.addFlashAttribute("flashMessageNotLocalized", e.getMessage());
    }

    return getRedirectString(subjectId);
  }

  @PostMapping("/lecturers/{lecturerId}/delete")
  public String removeLecturer(
      @PathVariable Long subjectId,
      @PathVariable Long lecturerId,
      RedirectAttributes redirectAttributes
  ) {

    try {
      LecturerEntity removed = subjectService.removeLecturerFromSubject(subjectId, lecturerId);

      String name = removed.getName();
      String msg = String.format("admin.subjects.lecturer.removed(${'%s'})", name);
      redirectAttributes.addFlashAttribute(FLASH_MSG, msg);

      return getRedirectString(subjectId);

    } catch (SubjectNotFoundException e) {
      String msgId = "admin.subjects.lecturer.subjectNotFound";
      redirectAttributes.addFlashAttribute(FLASH_MSG, msgId);

      return "redirect:/admin/subjects";

    } catch (LecturerNotFoundException e) {
      String msgId = "admin.subjects.lecturer.lecturerNotFound";
      redirectAttributes.addFlashAttribute(FLASH_MSG, msgId);

      return getRedirectString(subjectId);

    } catch (RelationNotfoundException e) {
      String msgId = "admin.subjects.lecturer.wasNoLecturer";
      redirectAttributes.addFlashAttribute(FLASH_MSG, msgId);

      return getRedirectString(subjectId);
    }
  }

  private String getRedirectString(Long subjectId) {
    return String.format("%s/%d", REDIRECT_ADMIN_SUBJECTS, subjectId);
  }

}
