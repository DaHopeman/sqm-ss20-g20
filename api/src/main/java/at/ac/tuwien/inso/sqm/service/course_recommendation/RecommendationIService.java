package at.ac.tuwien.inso.sqm.service.course_recommendation;

import at.ac.tuwien.inso.sqm.entity.Lehrveranstaltung;
import at.ac.tuwien.inso.sqm.entity.StudentEntity;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;

public interface RecommendationIService {

  /**
   * Recommends courses for a student by first filtering courses, running all the course scorers,
   * normalizing the results and scaling each scorer function by weight. Finally, the list is sorted
   * by score.
   *
   * <p>The user needs to be authenticated.
   *
   * @param student The student that needs recommendations
   * @return a sorted list of courses by weighted score
   */
  @PreAuthorize("isAuthenticated()")
  List<Lehrveranstaltung> recommendCourses(StudentEntity student);

  /**
   * Recommends courses, whereby the list contains no more than a number of entries defined as a
   * constant in the implementation.
   *
   * <p>The user needs to be authenticated.
   *
   * @return A sorted list of courses
   */
  @PreAuthorize("isAuthenticated()")
  List<Lehrveranstaltung> recommendCoursesSublist(StudentEntity student);
}
