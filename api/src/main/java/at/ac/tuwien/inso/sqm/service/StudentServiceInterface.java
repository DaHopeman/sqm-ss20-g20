package at.ac.tuwien.inso.sqm.service;

import at.ac.tuwien.inso.sqm.dto.SemesterDto;
import at.ac.tuwien.inso.sqm.entity.StduyPlanEntity;
import at.ac.tuwien.inso.sqm.entity.StudentEntity;
import at.ac.tuwien.inso.sqm.entity.StudyPlanRegistration;
import at.ac.tuwien.inso.sqm.entity.UserAccountEntity;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;

public interface StudentServiceInterface {

  /**
   * returns one student by id if he exists id should not be null and not <1
   */
  @PreAuthorize("isAuthenticated()")
  StudentEntity findOne(Long id);

  /**
   * returns one student by account user needs to be authenticated
   */
  @PreAuthorize("isAuthenticated()")
  StudentEntity findOne(UserAccountEntity account);

  /**
   * returns a student by its username user needs to be authenticated
   */
  @PreAuthorize("isAuthenticated()")
  StudentEntity findByUsername(String username);

  /**
   * returns all studyplanregistrations for the student. student should not be null! user needs to
   * be admin
   */
  @PreAuthorize("hasRole('ADMIN')")
  List<StudyPlanRegistration> findStudyPlanRegistrationsFor(StudentEntity student);

  /**
   * registers a student to a stduyplan for the current semester. student and stduyplan should not
   * be null may start a new semester
   *
   * <p>user needs to be admin
   */
  @PreAuthorize("hasRole('ADMIN')")
  void registerStudentToStudyPlan(StudentEntity student, StduyPlanEntity studyPlan);

  /**
   * registers a student to a stduyplan for the given semester student and stduyplan should not be
   * null
   *
   * <p>user needs to be admin!
   */
  @PreAuthorize("hasRole('ADMIN')")
  void registerStudentToStudyPlan(StudentEntity student, StduyPlanEntity studyPlan,
      SemesterDto currentSemester);
}
