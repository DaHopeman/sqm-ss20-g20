package at.ac.tuwien.inso.sqm.service.study_progress;

import at.ac.tuwien.inso.sqm.entity.StudentEntity;

public interface StudyProgressService {

  /**
   * Gibt den aktuellen Studienverlauf eines Studenten zurück
   */
  StudyProgress studyProgressFor(StudentEntity student);
}
